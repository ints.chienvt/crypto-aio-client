import { TAppExchange } from '@app/types/exchange';
import { DEFAULT_LANGUAGE, languages } from '../i18n';
import { TUser } from 'types/user';

export enum EKeyLS {
  KEY_LS_APP_EXCHANGES = 'APP_EXCHANGES_LS',
  KEY_LS_APP_BOTS = 'APP_BOTS_LS',
}

// Token LocalStorage
export const saveTokenLS = (token?: string) => {
  if (token) {
    localStorage.setItem('access_token', token);
  }
};

export const removeTokenLS = () => {
  localStorage.removeItem('access_token');
};

export const getTokenFromLS = () => {
  const token = localStorage.getItem('access_token');
  return token ? token : '';
};

// Refresh token LocalStorage
export const saveRefreshTokenLS = (refresh_token?: string) => {
  if (refresh_token) {
    localStorage.setItem('refresh_token', refresh_token);
  }
};

export const removeRefreshTokenLS = () => {
  localStorage.removeItem('refresh_token');
};

export const getRefreshTokenFromLS = () => {
  const refresh_token = localStorage.getItem('refresh_token');
  return refresh_token ? refresh_token : '';
};

// Save language to localStorage
export const saveLanguage = (language: string) => {
  if (languages.includes(language)) {
    localStorage.setItem('i18nextLng', language);
  } else {
    localStorage.setItem('i18nextLng', DEFAULT_LANGUAGE);
  }
};
export const getLanguage = () => {
  const language = localStorage.getItem('i18nextLng');
  return language && languages.includes(language) ? language : DEFAULT_LANGUAGE;
};

// Current User LocalStorage
export const saveUserLS = (data_user: TUser) => {
  localStorage.setItem('data_user', JSON.stringify(data_user));
};

export const removeUser = () => {
  localStorage.removeItem('data_user');
};

export const getUserFromLS = (): TUser => {
  let data = {};
  if (localStorage.getItem('data_user')) {
    try {
      const userLocal = localStorage.getItem('data_user');
      data = JSON.parse(userLocal ? userLocal : '');
    } catch (e) {
      data = {};
    }
  }
  return data;
};

// Save fcmToken to localStorage
export const saveFcmToken = (fcmToken?: string) => {
  !!fcmToken && localStorage.setItem('fcmToken', fcmToken);
};
export const getFcmToken = () => {
  const fcmToken = localStorage.getItem('fcmToken');
  return fcmToken ? fcmToken : '';
};
export const removeFcmToken = () => {
  localStorage.removeItem('fcmToken');
};

export const saveArrayLS = (dataArray: any[], key: EKeyLS) => {
  localStorage.setItem(key, JSON.stringify(dataArray));
};

export const getArrayFromLS = (key: EKeyLS): any[] => {
  let data = [];
  if (localStorage.getItem(key)) {
    try {
      const dataLS = localStorage.getItem(key);
      data = dataLS ? JSON.parse(dataLS) : [];
    } catch (e) {
      data = [];
    }
  }
  return data;
};

export const removeDataAuthInLS = () => {
  removeTokenLS();
  removeRefreshTokenLS();
  removeUser();
};
