import moment from 'moment';
import { IndexedObject, TOptionsQuery, TPagination } from 'types/common';

// Common
export const isMobile = window.screen.width < 768;

export const publicUrl = (uri: string) => {
  return `${process.env.PUBLIC_URL}${uri}`;
};

export const isEmptyObject = (obj: IndexedObject) => {
  if (obj.constructor === Object && Object.keys(obj).length === 0) {
    return true;
  }
  return JSON.stringify(obj) === JSON.stringify({});
};

export const isToday = (date: Date) => moment(date).isSame(moment(), 'day');

export const isMobileAgent = /(android|iphone|ipad|ipod)/i.test(window.navigator.userAgent);

export const DEFAULT_SIZE = 10;
export const DEFAULT_PAGE = 0;
export const DEFAULT_SIZE_GET_ALL = 999999;
export const DEFAULT_SORT = 'createdAt,desc';
export const DEFAULT_TOTAL_RESULTS = 0;

export const defaultPagination: TPagination = {
  size: DEFAULT_SIZE,
  page: DEFAULT_PAGE,
  totalResults: DEFAULT_TOTAL_RESULTS,
};

export const defaultParamsApiGetAll: TOptionsQuery = {
  size: DEFAULT_SIZE_GET_ALL,
  page: DEFAULT_PAGE,
  sort: DEFAULT_SORT,
};

export const defaultParamsApi: TOptionsQuery = {
  size: DEFAULT_SIZE,
  page: DEFAULT_PAGE,
  sort: DEFAULT_SORT,
};

export const DEFAULT_MAXIMUM_FRACTION_DIGITS_NUMBER = 20;
export const DEFAULT_FIXED_PRICE_USD = 8;
export const DEFAULT_FIXED_PERCENT = 4;
