import queryString from 'query-string';
import { Location } from 'react-router';
import { IndexedObject } from 'types/common';
import {
  DEFAULT_FIXED_PRICE_USD,
  DEFAULT_MAXIMUM_FRACTION_DIGITS_NUMBER,
  isEmptyObject,
} from './common';

export const omitObject = (obj: IndexedObject, keys: Array<any>): IndexedObject => {
  if (!keys.length) return obj;
  const { [keys.pop()]: omitted, ...rest } = obj;
  return omitObject(rest, keys);
};

export const replacePathParams = (path: string, params: IndexedObject<string>): string =>
  path.replace(/:([^/]+)/g, (_, p1) => encodeURIComponent(params[p1] ? params[p1] : ''));

export const parseFloatNum = (str: string) => {
  const trimmed = str && typeof str.trim === 'function' ? str.trim() : null;
  if (!trimmed) {
    return null;
  }
  const num = parseFloat(trimmed);
  const isNumber = !isNaN(num);
  const isFullyParsedNum = isNumber && num.toString() === trimmed;
  return isFullyParsedNum ? num : null;
};
export const parseQueryParams = (search: string) => {
  const params = queryString.parse(search);
  return Object.keys(params).reduce((result: IndexedObject, key) => {
    const val = params[key];
    if (val === 'true') {
      result[key] = true;
    } else if (val === 'false') {
      result[key] = false;
    } else {
      const num = parseFloatNum(val ? val.toString() : '');
      result[key] = num === null ? val : num;
    }
    return result;
  }, {});
};

export const getHashLocation = (location: Location) =>
  location.hash ? location.hash.replace('#', '') : '';

export const createQueryUrl = (location: IndexedObject, params: IndexedObject) => {
  const { pathname } = location;
  if (isEmptyObject(params)) return pathname;
  const query = queryString.stringify(params);
  return `${pathname}?${query}`;
};

export const percentCal = (cur: number | string, total: number | string, fixed?: number) => {
  if (!cur || !total) return 0;
  const per = (parseFloat(cur.toString()) / parseFloat(total.toString())) * 100;
  return per.toFixed(fixed ?? 0);
};

export const openUrlInNewTab = (url?: string) => {
  url && window.open(url, '_blank')?.focus();
};

export const isImage = (url: string) => {
  return /\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(url);
};

export const copyTextToClipboard = (text: string): boolean => {
  const textAreaDocument = document.createElement('textarea');
  textAreaDocument.style.position = 'fixed';
  textAreaDocument.style.top = '0';
  textAreaDocument.style.left = '0';
  textAreaDocument.style.width = '2em';
  textAreaDocument.style.height = '2em';
  textAreaDocument.style.padding = '0';
  textAreaDocument.style.border = 'none';
  textAreaDocument.style.outline = 'none';
  textAreaDocument.style.boxShadow = 'none';
  textAreaDocument.style.background = 'transparent';

  textAreaDocument.value = text;

  document.body.appendChild(textAreaDocument);
  textAreaDocument.focus();
  textAreaDocument.select();

  try {
    document.execCommand('copy');
    document.body.removeChild(textAreaDocument);
    return true;
  } catch (err) {
    document.body.removeChild(textAreaDocument);
    return false;
  }
};

const isObject = (object: any) => {
  return object !== null && typeof object === 'object';
};
export const isDeepEqualObject = (object1: IndexedObject, object2: IndexedObject) => {
  if (!isObject(object1) || !isObject(object2)) return false;
  const objKeys1 = Object.keys(object1);
  const objKeys2 = Object.keys(object2);

  if (objKeys1.length !== objKeys2.length) return false;

  for (const key of objKeys1) {
    const value1 = object1[key];
    const value2 = object2[key];

    const isObjects = isObject(value1) && isObject(value2);

    if ((isObjects && !isDeepEqualObject(value1, value2)) || (!isObjects && value1 !== value2)) {
      return false;
    }
  }
  return true;
};

export const reOrderArray: (list: any[], startIndex: number, endIndex: number) => any[] = (
  list: any[],
  startIndex: number,
  endIndex: number,
) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export const convertToURLString = (text?: string) => {
  return text ? text.replace('/', '__') : '';
};

export const convertStringFormURL = (text?: string) => {
  return text ? text.replace('__', '/') : '';
};

export const random6CodeString = () =>
  Math.floor(Math.random() * 1000000)
    .toString()
    .padStart(6, '0');

export const formatterPriceUSD = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
  maximumFractionDigits: DEFAULT_FIXED_PRICE_USD,
});

export const formattedNumber = (number?: number) =>
  Number(number ?? 0).toLocaleString('en-US', {
    minimumFractionDigits: 0,
    maximumFractionDigits: DEFAULT_MAXIMUM_FRACTION_DIGITS_NUMBER,
  });

export const shortenedStr = (str?: string, numberCharactersPrev?: number) => {
  const newNumberCharactersPrev = numberCharactersPrev ?? 4;
  return !str
    ? ''
    : str.length > newNumberCharactersPrev * 2 + 3
    ? `${str.slice(0, newNumberCharactersPrev)}...${str.slice(0 - newNumberCharactersPrev)}`
    : str;
};

export const getLastElementsOfArray = (arr: Array<any>, quantity: number) => {
  if (arr.length <= quantity) {
    return arr;
  } else {
    return arr.slice(-quantity);
  }
};

export const removeKeysUndefinedFormObj = (obj: IndexedObject) =>
  Object.keys(obj).reduce((acc: IndexedObject, key) => {
    if (obj[key] !== undefined) {
      acc[key] = obj[key];
    }
    return acc;
  }, {});
