import React, { useEffect } from 'react';
import { ConfigProvider } from 'antd';
import { HelmetProvider } from 'react-helmet-async';
import deDe from 'antd/lib/locale/de_DE';
import enUS from 'antd/lib/locale/en_US';
import GlobalStyle from './styles/GlobalStyle';
import 'typeface-montserrat';
import 'typeface-lato';
import { AppRouter } from './components/router/AppRouter';
import { useLanguage } from './hooks/useLanguage';
import { useAutoNightMode } from './hooks/useAutoNightMode';
import { usePWA } from './hooks/usePWA';
import { useThemeWatcher } from './hooks/useThemeWatcher';
import { useAppDispatch, useAppSelector } from './hooks/reduxHooks';
import { themeObject } from './styles/themes/themeVariables';
import { doGetAppExchanges } from './store/slices/exchangeSlice';
import { getTokenFromLS } from './services/localStorage';
import { doGetAppBots } from './store/slices/botSlice';

const App: React.FC = () => {
  const { language } = useLanguage();
  const dispatch = useAppDispatch();
  const theme = useAppSelector((state) => state.theme.theme);

  // usePWA();

  useAutoNightMode();

  useThemeWatcher();

  useEffect(() => {
    const tokenLS = getTokenFromLS();
    if (tokenLS) {
      dispatch(doGetAppExchanges());
      dispatch(doGetAppBots());
    }
  }, []);

  return (
    <>
      <meta name="theme-color" content={themeObject[theme].primary} />
      <GlobalStyle />
      <HelmetProvider>
        <ConfigProvider locale={language === 'en' ? enUS : deDe}>
          <AppRouter />
        </ConfigProvider>
      </HelmetProvider>
    </>
  );
};

export default App;
