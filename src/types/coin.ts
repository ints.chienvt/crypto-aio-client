import { TOrtherInfo } from './common';

export type TCoin = {
  id?: string;
  name?: string | null;
  symbol?: string | null;
  circulatingSupply?: number | null;
  totalSupply?: number | null;
  maxSupply?: number | null;
  infiniteSupply?: boolean | null;
  currentPrice?: number | null;
  volume24h?: number | null;
  volumeChange24h?: number | null;
  percentChange1h?: number | null;
  percentChange24h?: number | null;
  percentChange7d?: number | null;
  marketCap?: number | null;
  marketCapDominance?: number | null;
  fullyDilutedMarketCap?: number | null;
  active?: boolean | null;
} & TOrtherInfo;
