import { TCoinPair } from './coinPair';
import { TOrtherInfo } from './common';
import { EAppBotType } from './app-bot-type';
import { TAppExchange } from './exchange';
import { TUser } from './user';

export type TAppBot = {
  id?: string;
  name?: string | null;
  description?: string | null;
  botType?: keyof typeof EAppBotType | null;
  active?: boolean | null;
} & TOrtherInfo;

export type TUserBot = {
  id?: string;
  user?: TUser | null;
  appExchange?: TAppExchange | null;
  appBot?: TAppBot | null;
} & TOrtherInfo;

export type TBotGemHunting = {
  id?: string;
  price?: number | null;
  amount?: number | null;
  startTime?: Date | null;
  appCoinPair?: TCoinPair | null;
  userBot?: TUserBot | null;
} & TOrtherInfo;
