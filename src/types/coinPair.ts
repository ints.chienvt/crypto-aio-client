import { TAppExchange } from './exchange';

export type TDirectionCoinPair = {
  $color: 'long' | 'short';
};

export type TCoinPair = {
  id?: string;
  name?: string | null;
  symbol?: string | null;
  base?: string | null;
  quote?: string | null;
  active?: boolean | null;
  deleted?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
  appExchange?: TAppExchange | null;
};
