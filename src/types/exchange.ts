import { TOrtherInfo } from './common';
import { TUser } from './user';

export type TAppExchange = {
  id?: string;
  name?: string | null;
  logo?: string | null;
  description?: string | null;
  active?: boolean | null;
} & TOrtherInfo;

export type TUserExchange = {
  id?: string;
  accountName?: string | null;
  apiKey?: string | null;
  secretKey?: string | null;
  passPhrase?: string | null;
  isConnected?: boolean | null;
  user?: TUser | null;
  appExchange?: TAppExchange | null;
  balance?: number | null;
} & TOrtherInfo;
