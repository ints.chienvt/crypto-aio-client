import { TOrtherInfo } from './common';

export type TUser = {
  id?: string;
  login?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  activated?: boolean;
  langKey?: string;
  authorities?: string[];
  password?: string;
} & TOrtherInfo;

export type TUserInformation = {
  id?: string;
  oauthProvider?: string | null;
  oauthUid?: string | null;
  avatar?: string | null;
  isKYC?: boolean | null;
  otpCode?: string | null;
  lastSendingOtpAt?: Date | null;
  active?: boolean | null;
  user?: TUser | null;
} & TOrtherInfo;
