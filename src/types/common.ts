import { TFunction } from 'react-i18next';
import { NavigateFunction } from 'react-router-dom';

export type IndexedObject<V = any> = { [k: string]: V };

export type TQueryParams = { query?: string; page?: number; size?: number; sort?: string };

export type TAction<T> = {
  type: string;
  payload: IndexedObject<T>;
};

export type Locale = { code: string; name: string; bidi: boolean };

export type TOption<T = string | number> = {
  label: string;
  value?: T;
};

export type Role = 'ROLE_USER' | 'ROLE_ADMIN';

export type TokenObject = {
  token: string;
  expires: Date;
};

export type TPagination = {
  size: number;
  page: number;
  totalResults: number;
};

export type TResponsePagination<T = IndexedObject[]> = TPagination & {
  results: T;
};

export type TOptionsQuery<T = IndexedObject> = {
  sort?: string;
  size?: number;
  page?: number;
} & T;

export type TOrtherInfo = {
  createdBy?: string | null;
  createdDate?: Date | null;
  updatedBy?: string | null;
  updatedDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date;
  deleted?: boolean | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
};

export type TActionParams<T> = {
  navigate?: NavigateFunction;
  t?: TFunction<'translation', undefined>;
  funct?: (params?: any) => void;
  data: T;
};

export type TErrorApiResponse = {
  entityName: string;
  errorKey: string;
  type: string;
  title: string;
  status: number;
  message: string;
  params: string;
  detail: string;
};
