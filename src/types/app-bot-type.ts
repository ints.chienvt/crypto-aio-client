export enum EAppBotType {
  INTERNAL_USING = 'INTERNAL_USING',

  GEM_HUNTING = 'GEM_HUNTING',

  SPOT_DCA = 'SPOT_DCA',

  SPOT_GRID = 'SPOT_GRID',
}

export enum EAppCommandType {
  SPOT = 'spot',

  FUTURES = 'futures',

  ALL = 'all',
}

export enum EBotSetupTab {
  STRATEGY = 'strategy',

  BALANCE = 'balance',
}
