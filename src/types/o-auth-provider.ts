export enum OAuthProvider {
  NONE = 'NONE',

  GOOGLE = 'GOOGLE',

  FACEBOOK = 'FACEBOOK',
}
