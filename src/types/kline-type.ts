export enum KlineType {
  MIN_1 = 'MIN_1',

  MIN_5 = 'MIN_5',

  MIN_15 = 'MIN_15',

  MIN_30 = 'MIN_30',

  HOUR_1 = 'HOUR_1',

  HOUR_6 = 'HOUR_6',

  DAY_1 = 'DAY_1',

  DAY_3 = 'DAY_3',

  WEEK_1 = 'WEEK_1',

  MONTH_1 = 'MONTH_1',

  YEAR_1 = 'YEAR_1',
}
