import coinPairApi from '@app/api/coinPair.api';
import { defaultParamsApi } from '@app/services/common';
import { TCoinPair } from '@app/types/coinPair';
import { TOptionsQuery } from '@app/types/common';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export interface CoinPairSlice {
  loadingCoinPair: boolean;
  coinPairs: TCoinPair[];
  currentCoinPair: TCoinPair;
}

const initialState: CoinPairSlice = {
  loadingCoinPair: false,
  coinPairs: [],
  currentCoinPair: {},
};

export const doGetCoinPair = createAsyncThunk(
  'coinPair/doGetCoinPair',
  async (params?: TOptionsQuery<TCoinPair>) =>
    coinPairApi.getCoinPairs(params ?? defaultParamsApi).then((res) => {
      return res.data;
    }),
);

const coinPairSlice = createSlice({
  name: 'coinPair',
  initialState,
  reducers: {
    doSetCurrentCoinPair: (state, action) => {
      state.currentCoinPair = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doGetCoinPair.pending, (state) => {
      state.loadingCoinPair = true;
    });
    builder.addCase(doGetCoinPair.rejected, (state) => {
      state.loadingCoinPair = false;
    });
    builder.addCase(doGetCoinPair.fulfilled, (state, action) => {
      state.loadingCoinPair = false;
      state.coinPairs = action.payload;
    });
  },
});

export const { doSetCurrentCoinPair } = coinPairSlice.actions;
export default coinPairSlice.reducer;
