import userApi from '@app/api/user.api';
import { getUserFromLS, saveUserLS } from '@app/services/localStorage';
import { TUser } from '@app/types/user';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export interface UserState {
  user: TUser;
}

const initialState: UserState = {
  user: getUserFromLS(),
};

export const doGetCurrentUser = createAsyncThunk('user/doGetCurrentUser', async () =>
  userApi.getCurrentUser().then((res) => {
    return res.data;
  }),
);

export const doUpdateUser = createAsyncThunk('user/doUpdateUser', async (params: TUser) =>
  userApi.updateUser(params).then((res) => {
    return res.data;
  }),
);

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    doSetUser: (state, action) => {
      saveUserLS(action.payload);
      state.user = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doGetCurrentUser.fulfilled, (state, action) => {
      saveUserLS(action.payload);
      state.user = action.payload;
    });
    builder.addCase(doUpdateUser.fulfilled, (state, action) => {
      saveUserLS(action.payload);
      state.user = action.payload;
    });
  },
});

export const { doSetUser } = userSlice.actions;
export default userSlice.reducer;
