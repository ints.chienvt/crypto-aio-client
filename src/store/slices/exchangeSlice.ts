import exchangeApi, { TRequestAddExchange } from '@app/api/exchange.api';
import { defaultParamsApiGetAll } from '@app/services/common';
import { EKeyLS, getArrayFromLS, saveArrayLS } from '@app/services/localStorage';
import { TOptionsQuery } from '@app/types/common';
import { TAppExchange, TUserExchange } from '@app/types/exchange';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import botApi from '@app/api/bot.api';

export interface ExchangeSlice {
  loadingExchange: boolean;
  appExchanges: TAppExchange[];
  exchanges: TUserExchange[];
}

const initialState: ExchangeSlice = {
  loadingExchange: false,
  appExchanges: getArrayFromLS(EKeyLS.KEY_LS_APP_EXCHANGES),
  exchanges: [],
};

export const doGetAppExchanges = createAsyncThunk(
  'exchange/doGetAppExchanges',
  async (params?: TOptionsQuery<TAppExchange>) =>
    exchangeApi.getAppExchanges(params ?? defaultParamsApiGetAll).then((res) => {
      return res.data;
    }),
);

export const doGetExchanges = createAsyncThunk('exchange/doGetExchanges', async () =>
  exchangeApi.getExchanges().then((res) => {
    saveArrayLS(res.data, EKeyLS.KEY_LS_APP_EXCHANGES);
    return res.data;
  }),
);

export const doAddExchange = createAsyncThunk(
  'exchange/doAddExchange',
  async (body: TRequestAddExchange, { dispatch, getState }) =>
    exchangeApi.addExchange(body).then(async (res) => {
      const { appBots } = (getState() as RootState).bot;
      if (appBots?.length && res.data?.id) {
        for (const appBot of appBots) {
          await botApi.createUserBot({
            appBot: appBot?.id ?? '',
            appExchange: res.data.id,
          });
        }
      }
      dispatch(doGetExchanges());
      return res.data;
    }),
);

const exchangeSlice = createSlice({
  name: 'exchange',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(doGetExchanges.pending, (state) => {
      state.loadingExchange = true;
    });
    builder.addCase(doGetExchanges.rejected, (state) => {
      state.loadingExchange = false;
    });
    builder.addCase(doGetExchanges.fulfilled, (state, action) => {
      state.loadingExchange = false;
      state.exchanges = action.payload;
    });

    builder.addCase(doGetAppExchanges.fulfilled, (state, action) => {
      state.appExchanges = action.payload;
    });
  },
});

export const {} = exchangeSlice.actions;

export default exchangeSlice.reducer;
