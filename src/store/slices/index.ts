import userReducer from '@app/store/slices/userSlice';
import authReducer from '@app/store/slices/authSlice';
import nightModeReducer from '@app/store/slices/nightModeSlice';
import themeReducer from '@app/store/slices/themeSlice';
import pwaReducer from '@app/store/slices/pwaSlice';
import exchangeSlice from './exchangeSlice';
import coinPairSlice from './coinPairSlice';
import botSlice from './botSlice';

export default {
  user: userReducer,
  auth: authReducer,
  nightMode: nightModeReducer,
  theme: themeReducer,
  pwa: pwaReducer,
  exchange: exchangeSlice,
  coinPair: coinPairSlice,
  bot: botSlice,
};
