import botApi, { TRequestCreateBotGemHunting } from '@app/api/bot.api';
import { defaultParamsApiGetAll } from '@app/services/common';
import { EKeyLS, getArrayFromLS, saveArrayLS } from '@app/services/localStorage';
import { TAppBot, TBotGemHunting, TUserBot } from '@app/types/bot';
import { TOptionsQuery } from '@app/types/common';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export interface BotSlice {
  loadingBot: boolean;
  botActives: TBotGemHunting[];
  appBots: TAppBot[];
  userBots: TUserBot[];
  botCreated: TBotGemHunting;
}

const initialState: BotSlice = {
  loadingBot: false,
  botActives: [],
  appBots: getArrayFromLS(EKeyLS.KEY_LS_APP_BOTS),
  userBots: [],
  botCreated: {},
};

export const doGetAppBots = createAsyncThunk(
  'bot/doGetAppBots',
  async (params?: TOptionsQuery<TAppBot>) =>
    botApi.getAppBots(params ?? defaultParamsApiGetAll).then((res) => {
      saveArrayLS(res.data, EKeyLS.KEY_LS_APP_BOTS);
      return res.data;
    }),
);

export const doGetUserBots = createAsyncThunk(
  'bot/doGetUserBots',
  async (params?: TOptionsQuery<TUserBot>) =>
    botApi.getUserBots(params ?? defaultParamsApiGetAll).then((res) => {
      return res.data;
    }),
);

export const doGetBotActives = createAsyncThunk('bot/doGetBotActives', async () =>
  botApi.getBotActives().then((res) => {
    return res.data;
  }),
);

export const doCreateBotGemHunting = createAsyncThunk(
  'bot/doCreateBotGemHunting',
  async (body: TRequestCreateBotGemHunting, { dispatch }) =>
    botApi.createBotGemHunting(body).then((res) => {
      dispatch(doGetBotActives());
      return res.data;
    }),
);

const botSlice = createSlice({
  name: 'bot',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(doGetBotActives.pending, (state) => {
      state.loadingBot = true;
    });
    builder.addCase(doGetBotActives.rejected, (state) => {
      state.loadingBot = false;
    });
    builder.addCase(doGetBotActives.fulfilled, (state, action) => {
      state.loadingBot = false;
      state.botActives = action.payload;
    });

    builder.addCase(doGetAppBots.fulfilled, (state, action) => {
      state.loadingBot = false;
      state.appBots = action.payload;
    });

    builder.addCase(doGetUserBots.fulfilled, (state, action) => {
      state.loadingBot = false;
      state.userBots = action.payload;
    });

    builder.addCase(doCreateBotGemHunting.fulfilled, (state, action) => {
      state.botCreated = action.payload;
    });
  },
});

export default botSlice.reducer;
