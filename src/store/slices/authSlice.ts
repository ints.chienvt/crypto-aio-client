import authApi, { TLoginRequest, TLoginResponse, TSignUpRequest } from '@app/api/auth.api';
import userApi from '@app/api/user.api';
import {
  getTokenFromLS,
  removeDataAuthInLS,
  saveRefreshTokenLS,
  saveTokenLS,
} from '@app/services/localStorage';
import { doSetUser } from '@app/store/slices/userSlice';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export interface AuthSlice {
  token: string;
}

const initialState: AuthSlice = {
  token: getTokenFromLS(),
};

export const doLogin = createAsyncThunk(
  'auth/doLogin',
  async (loginPayload: TLoginRequest, { dispatch }) =>
    authApi.login(loginPayload).then(async (res) => {
      const data = (res as any).data as unknown as TLoginResponse;
      saveTokenLS(data.id_token);
      saveRefreshTokenLS(data.id_token);

      await userApi.getCurrentUser().then((res) => {
        dispatch(doSetUser((res as any).data));
      });

      return data.id_token;
    }),
);

export const doSignUp = createAsyncThunk(
  'auth/doSignUp',
  async (signUpPayload: TSignUpRequest, { dispatch }) =>
    authApi.signUp(signUpPayload).then(async (res) => {
      const data = res.data as unknown as TLoginResponse;
      saveTokenLS(data.id_token);
      saveRefreshTokenLS(data.id_token);

      await userApi.getCurrentUser().then((res) => {
        dispatch(doSetUser((res as any).data));
      });

      return data.id_token;
    }),
);

export const doLogout = createAsyncThunk('auth/doLogout', async (_, { dispatch }) => {
  removeDataAuthInLS();
  dispatch(doSetUser({}));
  dispatch(doSetToken(''));
  return authApi.logOut();
});

export const doResetPassword = createAsyncThunk('auth/doResetPassword', async () => {
  return;
});

export const doVerifySecurityCode = createAsyncThunk('auth/doVerifySecurityCode', async () => {
  return;
});

export const doSetNewPassword = createAsyncThunk('auth/doSetNewPassword', async () => {
  return;
});

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    doSetToken: (state, action) => {
      state.token = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(doLogin.fulfilled, (state, action) => {
      state.token = action.payload;
    });
  },
});

export const { doSetToken } = authSlice.actions;
export default authSlice.reducer;
