import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import translationEN from './locales/en/translation.json';
import translationDE from './locales/de/translation.json';
import { convertLanguageJsonToObject } from './locales/translations';

export const LANGUAGE_EN = 'en';
export const LANGUAGE_DE = 'de';

const resources = {
  [LANGUAGE_EN]: {
    translation: translationEN,
  },
  [LANGUAGE_DE]: {
    translation: translationDE,
  },
};

export const languages = Object.keys(resources);
export const DEFAULT_LANGUAGE = LANGUAGE_EN;

convertLanguageJsonToObject(translationEN);
convertLanguageJsonToObject(translationDE);

i18n.use(initReactI18next).init({
  resources,
  lng: LANGUAGE_EN,
  fallbackLng: DEFAULT_LANGUAGE,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
