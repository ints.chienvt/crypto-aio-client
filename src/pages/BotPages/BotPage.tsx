import { BotCommand } from '@app/components/bot/botCommand/BotCommand';
import { BotSetup } from '@app/components/bot/botSetup/BotSetup';
import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { PageTitle } from '@app/components/common/PageTitle/PageTitle';
import { useResponsive } from '@app/hooks/useResponsive';
import { translations } from '@app/locales/translations';
import React from 'react';
import { useTranslation } from 'react-i18next';
import * as S from './BotPage.styles';
import TradingViewWidget from '@app/components/TradingView/TradingView';
import { useAppSelector } from '@app/hooks/reduxHooks';

const BotPage: React.FC = () => {
  const { isDesktop } = useResponsive();
  const { t } = useTranslation();

  const { currentCoinPair } = useAppSelector((state) => state.coinPair);

  const desktopLayout = (
    <BaseRow>
      <S.LeftSideCol xl={16} xxl={17} id="desktop-content">
        <BaseRow gutter={[16, 16]}>
          <BaseCol xs={24} style={{ height: '480px' }}>
            <TradingViewWidget
              exchange={currentCoinPair.appExchange?.name?.toUpperCase() ?? 'BINANCE'}
              symbol={
                currentCoinPair.base
                  ? `${currentCoinPair.base}${currentCoinPair.symbol}`
                  : 'BTCUSDT'
              }
            />
          </BaseCol>
          <BaseCol xs={24}>
            <BotCommand />
          </BaseCol>
        </BaseRow>
      </S.LeftSideCol>

      <S.RightSideCol xl={8} xxl={7}>
        <BotSetup />
      </S.RightSideCol>
    </BaseRow>
  );

  const mobileAndTabletLayout = (
    <BaseRow gutter={[20, 24]}>
      {/* <BaseCol xs={24}>
        <BarAnimationDelayChart />
      </BaseCol> */}
      <BaseCol xs={24}>
        <BotCommand />
      </BaseCol>
      <BaseCol xs={24}>
        <BotSetup />
      </BaseCol>
    </BaseRow>
  );

  return (
    <>
      <PageTitle>{t(translations.COMMON.BOT)}</PageTitle>
      {isDesktop ? desktopLayout : mobileAndTabletLayout}
    </>
  );
};

export default BotPage;
