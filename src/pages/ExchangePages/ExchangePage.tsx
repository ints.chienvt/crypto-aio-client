import { PlusCircleOutlined } from '@ant-design/icons';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { BaseCard } from '@app/components/common/BaseCard/BaseCard';
import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { PageTitle } from '@app/components/common/PageTitle/PageTitle';
import { AddExchangeModal } from '@app/components/exchange/addExchangeModal/AddExchangeModal';
import { ExchangeCard } from '@app/components/exchange/exchangeCard/ExchangeCard';
import { useAppDispatch, useAppSelector } from '@app/hooks/reduxHooks';
import { translations } from '@app/locales/translations';
import { doGetExchanges } from '@app/store/slices/exchangeSlice';
import { LAYOUT } from '@app/styles/themes/constants';
import { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

const ExchangePage: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const [isLoading, setLoading] = useState<boolean>(false);
  const [isDemo, setDemo] = useState<boolean>(false);
  const [isShowModalAdd, setShowModalAdd] = useState<boolean>(false);
  const { appExchanges, exchanges } = useAppSelector((state) => state.exchange);

  useEffect(() => {
    setLoading(true);
    dispatch(doGetExchanges())
      .unwrap()
      .then(() => {
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  const exchange = useMemo(
    () =>
      exchanges.map((item) => (
        <BaseCol key={item.id} span={24} md={12} xl={8} xxl={6}>
          <ExchangeCard exchangeItem={item} isDemo={isDemo} />
        </BaseCol>
      )),
    [exchanges, isDemo],
  );

  return (
    <>
      <PageTitle>{t(translations.COMMON.EXCHANGE)}</PageTitle>
      <BaseCol>
        <ExchangePageCardStyled
          title={
            <BaseRow gutter={[40, 40]} wrap={false} justify="space-between" align="middle">
              <BaseCol>{t(translations.EXCHANGE.EXCHANGES)}</BaseCol>
              <BaseCol>
                <BaseButton
                  disabled={isLoading}
                  type="default"
                  htmlType="button"
                  onClick={() => setShowModalAdd(true)}
                  icon={<PlusCircleOutlined />}
                  size="middle"
                >
                  {t(translations.COMMON.ADD)}
                </BaseButton>
              </BaseCol>
            </BaseRow>
          }
        >
          <BaseRow gutter={[40, 40]} wrap={true} align="middle">
            {exchange}
          </BaseRow>
        </ExchangePageCardStyled>
      </BaseCol>
      <AddExchangeModal
        onCancel={() => setShowModalAdd(false)}
        visible={isShowModalAdd}
        appExchanges={appExchanges}
        exchanges={exchanges}
      />
    </>
  );
};

export default ExchangePage;

const ExchangePageCardStyled = styled(BaseCard)`
  min-height: calc(100vh - ${LAYOUT.desktop.headerHeight} - 1rem);
`;
