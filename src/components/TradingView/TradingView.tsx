import { useAppSelector } from '@app/hooks/reduxHooks';
import { useLanguage } from '@app/hooks/useLanguage';
import { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

let tvScriptLoadingPromise: Promise<unknown>;

const ID_TRADING_VIEW = 'tradingview_8276b';

type Props = {
  exchange: string;
  symbol: string;
  enable_publishing?: boolean;
  withdateranges?: boolean;
  hide_side_toolbar?: boolean;
  allow_symbol_change?: boolean;
  details?: boolean;
  hotlist?: boolean;
  calendar?: boolean;
};

export default function TradingViewWidget({
  exchange,
  symbol,
  enable_publishing = false,
  withdateranges = true,
  hide_side_toolbar = false,
  allow_symbol_change = true,
  details = false, // true
  hotlist = false, // true
  calendar = false, // true
}: Props) {
  const { language } = useLanguage();
  const { theme } = useAppSelector((state) => state.theme);
  const onLoadScriptRef = useRef<any>();

  useEffect(() => {
    function createWidget() {
      if (document.getElementById(ID_TRADING_VIEW) && 'TradingView' in window) {
        new ((window as any).TradingView as any).widget({
          autosize: true,
          symbol: `${exchange}:${symbol}`,
          timezone: 'Etc/UTC',
          theme: theme === 'dark' ? 'dark' : 'light',
          style: '1',
          locale: language,
          toolbar_bg: '#f1f3f6',
          enable_publishing: enable_publishing,
          withdateranges: withdateranges,
          range: '1D',
          hide_side_toolbar: hide_side_toolbar,
          allow_symbol_change: allow_symbol_change,
          details: details,
          hotlist: hotlist,
          calendar: calendar,
          container_id: ID_TRADING_VIEW,
        });
      }
    }

    onLoadScriptRef.current = createWidget;

    if (!tvScriptLoadingPromise) {
      tvScriptLoadingPromise = new Promise((resolve) => {
        const script = document.createElement('script');
        script.id = 'tradingview-widget-loading-script';
        script.src = 'https://s3.tradingview.com/tv.js';
        script.type = 'text/javascript';
        script.onload = resolve;

        document.head.appendChild(script);
      });
    }

    tvScriptLoadingPromise.then(() => onLoadScriptRef.current && onLoadScriptRef.current());

    return () => {
      onLoadScriptRef.current = null;
    };
  }, [exchange, symbol]);

  return (
    <div
      className="tradingview-widget-container"
      style={{
        position: 'relative',
        boxSizing: 'content-box',
        width: '100%',
        height: '100%',
        margin: ' 0 auto !important',
        padding: '0 !important',
      }}
    >
      <div
        id={ID_TRADING_VIEW}
        style={{
          width: '100%',
          height: '100%',
          background: 'transparent',
          padding: '0 !important',
        }}
      />
    </div>
  );
}
