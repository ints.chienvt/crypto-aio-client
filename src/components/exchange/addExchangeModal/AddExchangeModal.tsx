import { TRequestAddExchange } from '@app/api/exchange.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseModal } from '@app/components/common/BaseModal/BaseModal';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { BaseButtonsForm } from '@app/components/common/forms/BaseButtonsForm/BaseButtonsForm';
import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import { BaseInput } from '@app/components/common/inputs/BaseInput/BaseInput';
import { BaseSelect, Option } from '@app/components/common/selects/BaseSelect/BaseSelect';
import { useAppDispatch } from '@app/hooks/reduxHooks';
import { translations } from '@app/locales/translations';
import { doAddExchange } from '@app/store/slices/exchangeSlice';
import * as CustomStyle from '@app/styles/customStyle';
import { IndexedObject } from '@app/types/common';
import { TAppExchange, TUserExchange } from '@app/types/exchange';
import { defaultValidateMessages } from '@app/utils/validate/message';
import React, { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

type TAddExchangeModal = {
  appExchanges: TAppExchange[];
  exchanges: TUserExchange[];
  visible: boolean;
  onCancel: () => void;
};

const formItemLayout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 24 },
};

export const AddExchangeModal: React.FC<TAddExchangeModal> = ({
  appExchanges,
  exchanges,
  visible,
  onCancel,
}) => {
  const [isLoading, setLoading] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const [formAddExchange] = BaseForm.useForm();

  const currentAppExchangeIds = useMemo(() => exchanges.map((e) => e.appExchange?.id), [exchanges]);

  const onValuesChange = () => {
    setDisabledButton(false);
  };

  const onReset = () => {
    formAddExchange?.resetFields();
    setDisabledButton(true);
  };

  const onCloseModal = () => {
    onReset();
    onCancel();
  };

  const onFinish = async (values: TRequestAddExchange) => {
    setLoading(true);
    dispatch(doAddExchange(values))
      .unwrap()
      .then(() => {
        setLoading(false);
        onCloseModal();
      })
      .catch(() => {
        setLoading(false);
      });
  };

  return (
    <BaseModal
      title={t(translations.EXCHANGE.ADD_NEW_EXCHANGE)}
      centered
      visible={visible}
      onCancel={onCloseModal}
      size="medium"
      footer={null}
      destroyOnClose={true}
    >
      <BaseForm
        {...formItemLayout}
        name="add-exchange-form"
        validateMessages={defaultValidateMessages(t)}
        onFinish={onFinish}
        onFinishFailed={() => setDisabledButton(true)}
        onValuesChange={onValuesChange}
        form={formAddExchange}
        disabled={isLoading}
      >
        <BaseButtonsForm.Item
          name="appExchange"
          label={t(translations.COMMON.EXCHANGE)}
          rules={[{ required: true }]}
        >
          <BaseSelect>
            {appExchanges
              .filter((e) => !currentAppExchangeIds.includes(e.id))
              .map((e) => (
                <Option value={e.id} key={e.id}>
                  <BaseRow align="middle" gutter={[10, 0]} wrap={false}>
                    <BaseCol>
                      <img width={20} height={20} src={e.logo ?? ''} alt={e.name ?? ''} />
                    </BaseCol>
                    <BaseCol>
                      <CustomStyle.TextLineClamp>{e.name}</CustomStyle.TextLineClamp>
                    </BaseCol>
                  </BaseRow>
                </Option>
              ))}
          </BaseSelect>
        </BaseButtonsForm.Item>
        <BaseButtonsForm.Item name="apiKey" label={t(translations.EXCHANGE.API_KEY)}>
          <BaseInput />
        </BaseButtonsForm.Item>
        <BaseButtonsForm.Item name="secretKey" label={t(translations.EXCHANGE.SECRET_KEY)}>
          <BaseInput />
        </BaseButtonsForm.Item>
        <BaseButtonsForm.Item name="passPhrase" label={t(translations.EXCHANGE.PASSPHRASE)}>
          <BaseInput />
        </BaseButtonsForm.Item>
        <BaseRow justify="end" gutter={[40, 0]} style={{ paddingTop: '1rem' }}>
          <BaseCol>
            <BaseButton
              disabled={isLoading}
              type="default"
              htmlType="button"
              loading={isLoading}
              onClick={onReset}
            >
              {t(translations.COMMON.RESET)}
            </BaseButton>
          </BaseCol>
          <BaseCol>
            <BaseButton
              disabled={isLoading || disabledButton}
              type="primary"
              htmlType="submit"
              loading={isLoading}
            >
              {t(translations.EXCHANGE.ADD_NEW_EXCHANGE)}
            </BaseButton>
          </BaseCol>
        </BaseRow>
      </BaseForm>
    </BaseModal>
  );
};
