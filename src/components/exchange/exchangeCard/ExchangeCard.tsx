import { BaseAvatar } from '@app/components/common/BaseAvatar/BaseAvatar';
import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { CurrencyTypeEnum } from '@app/interfaces/interfaces';
import { translations } from '@app/locales/translations';
import { TUserExchange } from '@app/types/exchange';
import { formatNumberWithCommas, getCurrencyPrice } from '@app/utils/utils';
import React from 'react';
import { useTranslation } from 'react-i18next';
import * as S from './ExchangeCard.styles';

interface ExchangeCardProps {
  exchangeItem: TUserExchange;
  isDemo: boolean;
}

export const ExchangeCard: React.FC<ExchangeCardProps> = ({ exchangeItem, isDemo }) => {
  const { t } = useTranslation();

  return (
    <S.Card padding={0}>
      <S.ExchangeInfo>
        <S.Header gutter={[20, 20]} wrap={false} align="middle">
          <BaseCol>
            <BaseAvatar
              shape="circle"
              size={64}
              src={exchangeItem.appExchange?.logo}
              alt={exchangeItem.appExchange?.name ?? ''}
            />
          </BaseCol>
          <BaseCol>
            <S.Title>{exchangeItem.appExchange?.name}</S.Title>
            <S.InfoText>{exchangeItem.appExchange?.description}</S.InfoText>
            <S.InfoTextType $color={isDemo ? 'short' : 'long'}>
              {t(translations.COMMON[isDemo ? 'DEMO' : 'LIVE'])}
            </S.InfoTextType>
          </BaseCol>
        </S.Header>

        <S.InfoFooter>
          <S.CurrentBid>{t(translations.COMMON.BALANCE)}</S.CurrentBid>
          <S.Bid>
            {getCurrencyPrice(
              formatNumberWithCommas(exchangeItem.balance ?? 0),
              CurrencyTypeEnum.USD,
            )}
          </S.Bid>
        </S.InfoFooter>
      </S.ExchangeInfo>
    </S.Card>
  );
};
