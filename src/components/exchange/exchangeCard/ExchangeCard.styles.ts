import styled from 'styled-components';
import { NFTCard } from '@app/components/nft-dashboard/common/NFTCard/NFTCard';
import {
  FONT_SIZE,
  FONT_WEIGHT,
  FONT_FAMILY,
  media,
  BREAKPOINTS,
  BORDER_RADIUS,
} from '@app/styles/themes/constants';
import { BaseTypography } from '@app/components/common/BaseTypography/BaseTypography';
import { BaseCard } from '@app/components/common/BaseCard/BaseCard';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';

export const Title = styled(BaseTypography.Title)`
  position: relative;
  animation: titleOut 0.5s;

  &.ant-typography {
    margin-bottom: 0;

    font-size: ${FONT_SIZE.md};
  }
`;

export const ExchangeInfo = styled.div`
  padding: 1.25rem;
`;

export const InfoFooter = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const InfoText = styled.span`
  transition: all 0.5s ease;
  letter-spacing: 0.02em;

  font-weight: ${FONT_WEIGHT.regular};

  font-size: ${FONT_SIZE.xxs};

  font-family: ${FONT_FAMILY.secondary};

  color: var(--text-exchange-light-color);

  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  display: -webkit-box;

  @media only screen and ${media.xl} {
    font-size: ${FONT_SIZE.xs};
  }
`;

export const InfoTextType = styled(InfoText)<{ $color: string }>`
  color: ${(props) => `var(--${props.$color}-color)`};
  text-transform: uppercase;
  font-weight: ${FONT_WEIGHT.semibold};
`;

export const CurrentBid = styled(InfoText)`
  font-family: ${FONT_FAMILY.secondary};
  font-size: ${FONT_SIZE.xs};
  color: var(--text-main-color);

  @media only screen and ${media.xl} {
    font-size: ${FONT_SIZE.md};
  }
`;

export const Bid = styled(CurrentBid)`
  font-size: ${FONT_SIZE.xs};

  color: var(--text-main-color);

  font-weight: ${FONT_WEIGHT.semibold};

  font-family: ${FONT_FAMILY.main};

  @media only screen and ${media.xl} {
    font-size: ${FONT_SIZE.md};
  }
`;

export const Card = styled(BaseCard)`
  box-shadow: var(--box-shadow-nft-color);
  background: var(--additional-background-color);
`;

export const Header = styled(BaseRow)`
  margin-bottom: 1rem;
`;
