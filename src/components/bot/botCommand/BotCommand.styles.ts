import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import styled from 'styled-components';

export const WrapperTab = styled(BaseRow)`
  padding: 1rem;
`;
