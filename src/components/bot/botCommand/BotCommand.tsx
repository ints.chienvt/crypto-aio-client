import { BasicTableRow, Tag } from '@app/api/table.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { BaseSegmented } from '@app/components/common/BaseSegmented/BaseSegmented';
import { BaseSpace } from '@app/components/common/BaseSpace/BaseSpace';
import { BaseTable } from '@app/components/common/BaseTable/BaseTable';
import { useAppDispatch, useAppSelector } from '@app/hooks/reduxHooks';
import { translations } from '@app/locales/translations';
import { doGetBotActives } from '@app/store/slices/botSlice';
import { EAppCommandType } from '@app/types/app-bot-type';
import { ColumnsType } from 'antd/lib/table';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import { BaseCard } from '../../common/BaseCard/BaseCard';
import { Status } from '../botSetup/coinpair/CoinPairItem/CoinPairItem.styles';
import * as S from './BotCommand.styles';
import { TBotGemHunting } from '@app/types/bot';
import * as CustomStyle from '@app/styles/customStyle';
import moment from 'moment';
import { FORMAT_DATE_TIME_DEFAULT } from '@app/services/date';

enum EBotCommandTab {
  BOT_ACTIVE = 'bot_active',
  HISTORY = 'history',
}

export const BotCommand: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const optionTabs = [
    { label: t(translations.BOT.ACTIVE_BOTS), value: EBotCommandTab.BOT_ACTIVE },
    { label: t(translations.COMMON.HISTORY), value: EBotCommandTab.HISTORY, disabled: true },
  ];

  const optionTypes = [
    { label: t(translations.COMMON.ALL), value: EAppCommandType.ALL },
    { label: t(translations.COMMON.SPOT), value: EAppCommandType.SPOT, disabled: true },
    { label: t(translations.COMMON.FUTURES), value: EAppCommandType.FUTURES, disabled: true },
  ];

  const [valueBotCommandTab, setValueBotCommandTab] = useState<EBotCommandTab>(
    EBotCommandTab.BOT_ACTIVE,
  );
  const [valueBotCommandType, setValueBotCommandType] = useState<EAppCommandType>(
    EAppCommandType.ALL,
  );

  const [isLoading, setLoading] = useState<boolean>(false);

  const { botActives } = useAppSelector((state) => state.bot);

  useEffect(() => {
    setLoading(true);
    dispatch(doGetBotActives())
      .unwrap()
      .then(() => {
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  const columns: ColumnsType<TBotGemHunting> = [
    {
      title: t(translations.COMMON.name),
      render: (_, record: TBotGemHunting) => (
        <CustomStyle.TextLineClamp>{record.appCoinPair?.name}</CustomStyle.TextLineClamp>
      ),
    },
    {
      title: t(translations.COMMON.PRICE),
      dataIndex: 'price',
    },
    {
      title: t(translations.COMMON.AMOUNT),
      dataIndex: 'amount',
    },
    {
      title: t(translations.BOT.BOT_TYPE),
      render: (_, record: TBotGemHunting) =>
        t(`BOT.${record.userBot?.appBot?.botType ?? 'SPOT_DCA'}`),
    },
    {
      title: t(translations.BOT.START_TIME),
      render: (_, record: TBotGemHunting) =>
        moment(record.startTime).format(FORMAT_DATE_TIME_DEFAULT),
    },
    // {
    //   title: t('COMMON.tags'),
    //   key: 'tags',
    //   dataIndex: 'tags',
    //   render: (tags: Tag[]) => (
    //     <BaseRow gutter={[10, 10]}>
    //       {tags.map((tag: Tag) => {
    //         return (
    //           <BaseCol key={tag.value}>
    //             <Status $color={'long'}>Long</Status>
    //           </BaseCol>
    //         );
    //       })}
    //     </BaseRow>
    //   ),
    // },
    // {
    //   title: t('tables.actions'),
    //   dataIndex: 'actions',
    //   width: '8%',
    //   render: () => {
    //     return (
    //       <BaseSpace>
    //         <BaseButton type="default" danger>
    //           {t('tables.delete')}
    //         </BaseButton>
    //       </BaseSpace>
    //     );
    //   },
    // },
  ];

  return (
    <BotCommandStyled padding={0}>
      <S.WrapperTab gutter={[40, 20]} align="middle">
        <BaseCol>
          <BaseSegmented
            options={optionTabs}
            value={valueBotCommandTab}
            onChange={(value) => setValueBotCommandTab(value as EBotCommandTab)}
            disabled={isLoading}
          />
        </BaseCol>
        <BaseCol>
          <BaseSegmented
            options={optionTypes}
            value={valueBotCommandType}
            onChange={(value) => setValueBotCommandType(value as EAppCommandType)}
            disabled={isLoading}
          />
        </BaseCol>
      </S.WrapperTab>
      <BaseTable
        columns={columns}
        dataSource={botActives}
        pagination={false}
        loading={isLoading}
        scroll={{ x: 680 }}
      />
    </BotCommandStyled>
  );
};

const BotCommandStyled = styled(BaseCard)`
  height: 100%;
  .ant-pagination {
    margin-right: 1rem;
  }
`;
