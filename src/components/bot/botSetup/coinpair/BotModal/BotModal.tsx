import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseModal } from '@app/components/common/BaseModal/BaseModal';
import { BaseRadio } from '@app/components/common/BaseRadio/BaseRadio';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { BaseButtonsForm } from '@app/components/common/forms/BaseButtonsForm/BaseButtonsForm';
import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import { InputNumber } from '@app/components/common/inputs/InputNumber/InputNumber.styles';
import { BaseDatePicker } from '@app/components/common/pickers/BaseDatePicker';
import { BaseSelect, Option } from '@app/components/common/selects/BaseSelect/BaseSelect';
import { useAppDispatch } from '@app/hooks/reduxHooks';
import { translations } from '@app/locales/translations';
import { formattedNumber, removeKeysUndefinedFormObj } from '@app/services/functions';
import { doCreateBotGemHunting } from '@app/store/slices/botSlice';
import { TAppBot, TUserBot } from '@app/types/bot';
import { TCoinPair } from '@app/types/coinPair';
import { IndexedObject } from '@app/types/common';
import { checkPositiveNumber } from '@app/utils/validate';
import { defaultValidateMessages } from '@app/utils/validate/message';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import * as S from '../CoinPairItem/CoinPairItem.styles';
import { TRequestCreateBotGemHunting } from '@app/api/bot.api';

type TBotModal = {
  coinPair: TCoinPair | null;
  visible: boolean;
  onCancel: () => void;
  appBots: TAppBot[];
};

const formItemLayout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 24 },
};

export const BotModal: React.FC<TBotModal> = ({ coinPair, visible, onCancel, appBots }) => {
  const [isLoading, setLoading] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const [formBot] = BaseForm.useForm();

  if (!coinPair) {
    return null;
  }

  const onValuesChange = () => {
    setDisabledButton(false);
  };

  const onReset = () => {
    formBot?.resetFields();
    setDisabledButton(true);
  };

  const onCloseModal = () => {
    onReset();
    onCancel();
  };

  const onFinish = async (values: TRequestCreateBotGemHunting) => {
    setLoading(true);
    const newValues = removeKeysUndefinedFormObj({
      ...values,
      appCoinPair: coinPair?.id ?? '',
    }) as TRequestCreateBotGemHunting;
    dispatch(doCreateBotGemHunting(newValues))
      .unwrap()
      .then(() => {
        setLoading(false);
        onCloseModal();
      })
      .catch(() => {
        setLoading(false);
      });
  };

  return (
    <BaseModal
      title={t(translations.BOT.CREATE_BOT)}
      centered
      visible={visible}
      onCancel={onCloseModal}
      size="large"
      footer={null}
      destroyOnClose={true}
    >
      <BaseForm
        {...formItemLayout}
        name="bot-setup-form"
        validateMessages={defaultValidateMessages(t)}
        onFinish={onFinish}
        onFinishFailed={() => setDisabledButton(true)}
        onValuesChange={onValuesChange}
        form={formBot}
        disabled={isLoading}
      >
        <BaseRow gutter={[40, 0]} justify="space-between" align="top">
          <BaseCol span={24} md={8}>
            <BaseButtonsForm.Item label={t(translations.BOT.COIN_PAIR)}>
              <BaseSelect disabled defaultValue="coinPair">
                <Option value="coinPair">
                  <S.TitleModalCoinPair>{coinPair.name}</S.TitleModalCoinPair>
                </Option>
              </BaseSelect>
            </BaseButtonsForm.Item>
          </BaseCol>
          <BaseCol span={24} md={8}>
            <BaseButtonsForm.Item label={t(translations.COMMON.EXCHANGE)}>
              <BaseSelect disabled defaultValue="exchange">
                <Option value="exchange">
                  <S.Wrapper>
                    <img
                      width={20}
                      height={20}
                      src={coinPair.appExchange?.logo ?? ''}
                      alt={coinPair.appExchange?.name ?? ''}
                    />
                    <S.TitleModalCoinPair>{coinPair.appExchange?.name}</S.TitleModalCoinPair>
                  </S.Wrapper>
                </Option>
              </BaseSelect>
            </BaseButtonsForm.Item>
          </BaseCol>
          <BaseCol span={24} md={8}>
            <BaseButtonsForm.Item
              name="appBot"
              label={t(translations.BOT.BOT_TYPE)}
              rules={[{ required: true }]}
            >
              <BaseSelect>
                {appBots.map((appBot) => (
                  <Option value={appBot.id} key={appBot.id}>
                    <S.TitleModalCoinPair>{t(`BOT.${appBot?.botType}`)}</S.TitleModalCoinPair>
                  </Option>
                ))}
              </BaseSelect>
            </BaseButtonsForm.Item>
          </BaseCol>
        </BaseRow>
        <BaseRow gutter={[40, 0]} justify="space-between" align="top">
          <BaseCol span={24} md={12}>
            <BaseButtonsForm.Item
              name="direction"
              label={t(translations.BOT.DIRECTION)}
              rules={[{ required: true }]}
            >
              <BaseRadio.Group className="w-full flex-center-center">
                <BaseRadio.DirectionButton className="w-half flex-center-center long" value="long">
                  {t(translations.COMMON.LONG)}
                </BaseRadio.DirectionButton>
                <BaseRadio.DirectionButton
                  className="w-half flex-center-center short"
                  value="short"
                >
                  {t(translations.COMMON.SHORT)}
                </BaseRadio.DirectionButton>
              </BaseRadio.Group>
            </BaseButtonsForm.Item>
          </BaseCol>
          <BaseCol span={24} md={12}>
            <BaseButtonsForm.Item name="startTime" label={t(translations.BOT.START_TIME)}>
              <BaseDatePicker className="w-full" showTime />
            </BaseButtonsForm.Item>
          </BaseCol>
        </BaseRow>
        <BaseRow gutter={[40, 0]} justify="space-between" align="top">
          <BaseCol span={24} md={12}>
            <BaseButtonsForm.Item
              name="investment"
              label={t(translations.BOT.INVESTMENT)}
              rules={[
                { required: true },
                {
                  type: 'number',
                },
                {
                  pattern: checkPositiveNumber,
                  message: t(translations.VALIDATE.CUSTOM.VALIDATE_POSITIVE_NUMBER),
                },
              ]}
            >
              <InputNumber
                className="w-full"
                prefix={coinPair.quote ?? '$'}
                step="any"
                formatter={(value) => (value ? formattedNumber(Number(value)) : '')}
              />
            </BaseButtonsForm.Item>
            {/* <BaseSlider
              marks={{
                0: '0%',
                20: '20%',
                40: '40%',
                60: '60%',
                80: '80%',
                100: '100%',
              }}
              tooltipVisible={false}
            /> */}
          </BaseCol>
          <BaseCol span={24} md={12}>
            <BaseButtonsForm.Item
              name="price"
              label={t(translations.COMMON.PRICE)}
              rules={[
                {
                  type: 'number',
                },
                {
                  pattern: checkPositiveNumber,
                  message: t(translations.VALIDATE.CUSTOM.VALIDATE_POSITIVE_NUMBER),
                },
              ]}
            >
              <InputNumber
                className="w-full"
                prefix={coinPair.quote ?? '$'}
                step="any"
                formatter={(value) => (value ? formattedNumber(Number(value)) : '')}
              />
            </BaseButtonsForm.Item>
          </BaseCol>
        </BaseRow>
        <BaseRow justify="end" gutter={[40, 0]} style={{ paddingTop: '1rem' }}>
          <BaseCol>
            <BaseButton
              disabled={isLoading}
              type="default"
              htmlType="button"
              loading={isLoading}
              onClick={onReset}
            >
              {t(translations.COMMON.RESET)}
            </BaseButton>
          </BaseCol>
          <BaseCol>
            <BaseButton
              disabled={isLoading || disabledButton}
              type="primary"
              htmlType="submit"
              loading={isLoading}
            >
              {t(translations.BOT.CREATE_BOT)}
            </BaseButton>
          </BaseCol>
        </BaseRow>
      </BaseForm>
    </BaseModal>
  );
};
