import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { TCoinPair } from '@app/types/coinPair';
import React from 'react';
import { useTranslation } from 'react-i18next';
import * as S from './CoinPairItem.styles';

type TCoinPairItemProps = {
  coinPair: TCoinPair;
};

export const CoinPairItem: React.FC<TCoinPairItemProps> = ({ coinPair }) => {
  const { t } = useTranslation();

  // const coinPairDirection = coinPairDirections.find(
  //   (configStatus) => configStatus.name === direction,
  // );

  return (
    <S.CoinPairItem gutter={[20, 20]} wrap={false} justify="space-between" align="middle">
      <BaseCol span={10}>
        <S.Wrapper>
          <img
            width={20}
            height={20}
            src={coinPair.appExchange?.logo ?? ''}
            alt={coinPair.appExchange?.name ?? ''}
          />
          <S.Title>{coinPair.name}</S.Title>
        </S.Wrapper>
      </BaseCol>

      <BaseCol span={7}>
        <BaseRow justify="center">
          {/* <S.Status $color={coinPairDirection?.color || 'long'}>
            {t(coinPairDirection?.title || '')}
          </S.Status> */}
          <S.TextCoinPairSpot>{coinPair.base}</S.TextCoinPairSpot>
        </BaseRow>
      </BaseCol>
      <BaseCol span={7}>
        <BaseRow justify="end">
          <S.TextCoinPair>{coinPair.appExchange?.name}</S.TextCoinPair>
        </BaseRow>
      </BaseCol>
    </S.CoinPairItem>
  );
};
