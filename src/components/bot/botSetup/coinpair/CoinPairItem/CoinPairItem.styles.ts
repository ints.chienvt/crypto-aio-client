import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { BaseTypography } from '@app/components/common/BaseTypography/BaseTypography';
import { FONT_FAMILY, FONT_SIZE, FONT_WEIGHT } from '@app/styles/themes/constants';
import { TDirectionCoinPair } from '@app/types/coinPair';
import styled from 'styled-components';

export const Title = styled(BaseTypography.Text)`
  font-size: ${FONT_SIZE.xs};
  font-weight: ${FONT_WEIGHT.semibold};
  font-family: ${FONT_FAMILY.secondary};
`;

export const Wrapper = styled(BaseRow)`
  gap: 10px;
  align-items: center;
`;

export const CoinPairItem = styled(BaseRow)`
  cursor: pointer;
`;

export const Status = styled(BaseTypography.Text)<TDirectionCoinPair>`
  color: ${(props) => `var(--${props.$color}-color)`};
  background-color: ${(props) => `var(--${props.$color}-background-color)`};
  font-size: ${FONT_SIZE.xxs};
  font-family: ${FONT_FAMILY.secondary};
  display: block;
  padding: 3px 8px;
  border-radius: 4px;
  text-align: center;
  min-width: 46px;
`;

export const TextCoinPair = styled(Title)`
  font-weight: ${FONT_WEIGHT.regular};
`;

export const TextCoinPairSpot = styled(TextCoinPair)`
  color: var(--violet1);
`;

export const TitleModalCoinPair = styled(Title)`
  font-weight: ${FONT_WEIGHT.semibold};
  font-size: ${FONT_SIZE.md};
`;

export const Text = styled(BaseTypography.Text)`
  font-size: ${FONT_SIZE.xs};

  font-weight: ${FONT_WEIGHT.regular};

  font-family: ${FONT_FAMILY.secondary};
`;
