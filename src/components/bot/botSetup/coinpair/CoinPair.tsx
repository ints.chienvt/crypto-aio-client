import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { translations } from '@app/locales/translations';
import { TCoinPair } from '@app/types/coinPair';
import React, { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { BotModal } from './BotModal/BotModal';
import * as S from './CoinPair.styles';
import { CoinPairItem } from './CoinPairItem/CoinPairItem';
import { useAppDispatch, useAppSelector } from '@app/hooks/reduxHooks';
import { doSetCurrentCoinPair } from '@app/store/slices/coinPairSlice';
import { TAppBot, TUserBot } from '@app/types/bot';

type CoinPairProps = {
  coinPairs: TCoinPair[];
  appBots: TAppBot[];
};

export const CoinPair: React.FC<CoinPairProps> = ({ coinPairs, appBots }) => {
  const { currentCoinPair } = useAppSelector((state) => state.coinPair);
  const [isOpenModal, setOpenModal] = useState<boolean>(false);
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const coinPair = useMemo(
    () =>
      coinPairs.map((item, index) => (
        <BaseCol
          key={index}
          span={24}
          onClick={() => {
            dispatch(doSetCurrentCoinPair(item));
            setOpenModal(true);
          }}
        >
          <CoinPairItem coinPair={item} />
        </BaseCol>
      )),
    [coinPairs],
  );

  return (
    <S.Wrapper>
      <BaseRow gutter={[20, 20]} wrap={false} justify="space-between" align="middle">
        <BaseCol span={10}>
          <S.Title level={2}>{t(translations.BOT.EX_COIN_PAIR)}</S.Title>
        </BaseCol>
        <BaseCol span={7}>
          {/* <S.TitleCenter level={2}>{t(translations.BOT.DIRECTION)}</S.TitleCenter> */}
          {/* <S.TitleCenter level={2}>{t(translations.BOT.BOT_TYPE)}</S.TitleCenter> */}
          <S.TitleCenter level={2}>{t(translations.COMMON.BASE)}</S.TitleCenter>
        </BaseCol>
        <BaseCol span={7}>
          {/* <S.TitleRight level={2}>{t(translations.BOT.PROFIT)}</S.TitleRight> */}
          <S.TitleRight level={2}>{t(translations.COMMON.EXCHANGE)}</S.TitleRight>
        </BaseCol>
      </BaseRow>
      <S.CoinPairRow gutter={[20, 20]}>{coinPair}</S.CoinPairRow>

      <BotModal
        coinPair={currentCoinPair}
        onCancel={() => setOpenModal(false)}
        visible={isOpenModal}
        appBots={appBots}
      />
    </S.Wrapper>
  );
};
