import { BaseSegmented } from '@app/components/common/BaseSegmented/BaseSegmented';
import { SearchInput } from '@app/components/common/inputs/SearchInput/SearchInput.styles';
import { useAppDispatch, useAppSelector } from '@app/hooks/reduxHooks';
import { translations } from '@app/locales/translations';
import { doGetCoinPair } from '@app/store/slices/coinPairSlice';
import { TCoinPair } from '@app/types/coinPair';
import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import * as S from './BotSetup.styles';
import { CoinPair } from './coinpair/CoinPair';
import { EBotSetupTab } from '@app/types/app-bot-type';
import { doGetUserBots } from '@app/store/slices/botSlice';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useNavigate } from 'react-router-dom';
import { PlusCircleOutlined } from '@ant-design/icons';
import { EPath } from '@app/components/router/routerConfig';

export const BotSetup: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const optionsBotSetupTab = [
    { label: t(translations.BOT.STRATEGIES), value: EBotSetupTab.STRATEGY },
    { label: t(translations.COMMON.BALANCE), value: EBotSetupTab.BALANCE, disabled: true },
  ];

  const [coinPairSearchs, setCoinPairSearchs] = useState<TCoinPair[]>([]);
  const [isLoading, setLoading] = useState<boolean>(false);
  const { coinPairs } = useAppSelector((state) => state.coinPair);
  const { appBots } = useAppSelector((state) => state.bot);
  const [valueBotSetupTab, setValueBotSetupTab] = useState<EBotSetupTab>(EBotSetupTab.STRATEGY);

  useEffect(() => {
    setLoading(true);
    Promise.all([dispatch(doGetCoinPair()).unwrap(), dispatch(doGetUserBots()).unwrap()])
      .then(() => {
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    setCoinPairSearchs(coinPairs);
  }, [coinPairs]);

  const onSearch = (value: string) => {
    const newCoinPairSearchs = value
      ? coinPairs.filter((c) =>
          [c.name, c.symbol, c.base].join('').toLowerCase().includes(value.toLowerCase()),
        )
      : coinPairs;
    setCoinPairSearchs(newCoinPairSearchs);
  };

  return (
    <S.Wrapper id="activity">
      <BaseSegmented
        options={optionsBotSetupTab}
        block
        value={valueBotSetupTab}
        onChange={(value) => setValueBotSetupTab(value as EBotSetupTab)}
        disabled={isLoading}
      />
      {appBots.length ? (
        <>
          <SearchInput
            placeholder={t(translations.COMMON.SEARCH_PLACEHOLDER)}
            allowClear
            onSearch={onSearch}
            disabled={isLoading}
          />
          <S.ScrollWrapper id="activity-story">
            <CoinPair coinPairs={coinPairSearchs} appBots={appBots} />
          </S.ScrollWrapper>
        </>
      ) : (
        <BaseButton
          loading={isLoading}
          type="default"
          htmlType="button"
          onClick={() => navigate(EPath.EXCHANGE)}
          icon={<PlusCircleOutlined />}
        >
          {t(isLoading ? translations.COMMON.loading : translations.EXCHANGE.ADD_EXCHANGE)}
        </BaseButton>
      )}
    </S.Wrapper>
  );
};
