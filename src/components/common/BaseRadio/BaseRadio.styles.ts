import styled from 'styled-components';
import { Radio as AntdRadio } from 'antd';

export const Radio = styled(AntdRadio)`
  .ant-radio-disabled + span {
    color: var(--disabled-color);
  }
`;

export const RadioButton = styled(AntdRadio.Button)`
  &.ant-radio-button-wrapper-disabled {
    color: var(--disabled-color);
  }
`;

export const DirectionButton = styled(RadioButton)`
  &.long.ant-radio-button-wrapper {
    &:hover {
      color: var(--long-color);
    }
  }
  &.long.ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
    color: var(--long-color);
    border-color: var(--long-color);
    &::before,
    .ant-spin-dot-item {
      background-color: var(--long-color);
    }
    &:hover {
      color: var(--long-color);
    }
  }
  &.short.ant-radio-button-wrapper {
    &:hover {
      color: var(--short-color);
    }
  }
  &.short.ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
    color: var(--short-color);
    border-color: var(--short-color);
    &::before,
    .ant-spin-dot-item {
      background-color: var(--short-color);
    }
    &:hover {
      color: var(--short-color);
    }
  }
`;
