import styled from 'styled-components';
import { Segmented as AntdSegmented } from 'antd';

export const Segmented = styled(AntdSegmented)`
  .ant-segmented-item.ant-segmented-item-disabled {
    color: var(--disabled-color);
  }
`;
