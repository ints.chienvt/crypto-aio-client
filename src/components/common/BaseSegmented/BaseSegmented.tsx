import { Segmented as AntdSegmented, SegmentedProps } from 'antd';

export type BaseSegmentedProps = SegmentedProps;

export const BaseSegmented: React.FC<BaseSegmentedProps> = ({ ...props }) => {
  return <AntdSegmented {...props} />;
};
