import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import { useAppDispatch } from '@app/hooks/reduxHooks';
import { doSignUp } from '@app/store/slices/authSlice';
import { notificationController } from '@app/controllers/notificationController';
import { ReactComponent as GoogleIcon } from '@app/assets/icons/google.svg';
import { ReactComponent as FacebookIcon } from '@app/assets/icons/facebook.svg';
import * as Auth from '@app/components/layouts/AuthLayout/AuthLayout.styles';
import * as S from './SignUpForm.styles';
import { EPath } from '@app/components/router/routerConfig';
import { defaultValidateMessages } from '@app/utils/validate/message';
import { omitObject } from '@app/services/functions';
import { TSignUpRequest } from '@app/api/auth.api';
import { AxiosError } from 'axios';

interface SignUpFormData {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export const SignUpForm: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [isLoading, setLoading] = useState(false);

  const { t } = useTranslation();

  const handleSubmit = (values: SignUpFormData) => {
    const newValues = omitObject(values, ['confirmPassword']) as TSignUpRequest;
    console.log('newValues', newValues);
    setLoading(true);
    dispatch(doSignUp(newValues))
      .unwrap()
      .then(() => {
        notificationController.success({
          message: t('auth.signUpSuccessMessage'),
          description: t('auth.signUpSuccessDescription'),
        });
        navigate(EPath.LOGIN);
      })
      .catch((err: AxiosError) => {
        notificationController.error({
          message: err.message,
          duration: 100000,
        });
        setLoading(false);
      });
  };

  return (
    <Auth.FormWrapper>
      <BaseForm
        layout="vertical"
        onFinish={handleSubmit}
        requiredMark="optional"
        validateMessages={defaultValidateMessages(t)}
        disabled={isLoading}
      >
        <S.Title>{t('COMMON.signUp')}</S.Title>
        <Auth.FormItem name="login" label={t('COMMON.username')} rules={[{ required: true }]}>
          <Auth.FormInput placeholder={t('COMMON.username')} />
        </Auth.FormItem>
        <Auth.FormItem name="firstName" label={t('COMMON.firstName')} rules={[{ required: true }]}>
          <Auth.FormInput placeholder={t('COMMON.firstName')} />
        </Auth.FormItem>
        <Auth.FormItem name="lastName" label={t('COMMON.lastName')} rules={[{ required: true }]}>
          <Auth.FormInput placeholder={t('COMMON.lastName')} />
        </Auth.FormItem>
        <Auth.FormItem
          name="email"
          label={t('COMMON.email')}
          rules={[
            { required: true },
            {
              type: 'email',
              message: t('COMMON.notValidEmail'),
            },
          ]}
        >
          <Auth.FormInput placeholder={t('COMMON.email')} />
        </Auth.FormItem>
        <Auth.FormItem label={t('COMMON.password')} name="password" rules={[{ required: true }]}>
          <Auth.FormInputPassword placeholder={t('COMMON.password')} />
        </Auth.FormItem>
        <Auth.FormItem
          label={t('COMMON.confirmPassword')}
          name="confirmPassword"
          dependencies={['password']}
          rules={[
            { required: true },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error(t('COMMON.confirmPasswordError')));
              },
            }),
          ]}
        >
          <Auth.FormInputPassword placeholder={t('COMMON.confirmPassword')} />
        </Auth.FormItem>
        {/* <Auth.ActionsWrapper>
          <BaseForm.Item name="termOfUse" valuePropName="checked" noStyle>
            <Auth.FormCheckbox>
              <Auth.Text>
                {t('signup.agree')}{' '}
                <Link to="#" target={'_blank'}>
                  <Auth.LinkText>{t('signup.termOfUse')}</Auth.LinkText>
                </Link>{' '}
                and{' '}
                <Link to="#" target={'_blank'}>
                  <Auth.LinkText>{t('signup.privacyOPolicy')}</Auth.LinkText>
                </Link>
              </Auth.Text>
            </Auth.FormCheckbox>
          </BaseForm.Item>
        </Auth.ActionsWrapper> */}
        <BaseForm.Item noStyle>
          <Auth.SubmitButton
            type="primary"
            htmlType="submit"
            loading={isLoading}
            style={{ marginTop: '2rem' }}
          >
            {t('COMMON.signUp')}
          </Auth.SubmitButton>
        </BaseForm.Item>
        {/* <BaseForm.Item noStyle>
          <Auth.SocialButton type="default" htmlType="submit">
            <Auth.SocialIconWrapper>
              <GoogleIcon />
            </Auth.SocialIconWrapper>
            {t('signup.googleLink')}
          </Auth.SocialButton>
        </BaseForm.Item>
        <BaseForm.Item noStyle>
          <Auth.SocialButton type="default" htmlType="submit">
            <Auth.SocialIconWrapper>
              <FacebookIcon />
            </Auth.SocialIconWrapper>
            {t('signup.facebookLink')}
          </Auth.SocialButton>
        </BaseForm.Item> */}
        <Auth.FooterWrapper>
          <Auth.Text>
            {t('signup.alreadyHaveAccount')}{' '}
            <Link to={EPath.LOGIN}>
              <Auth.LinkText>{t('COMMON.here')}</Auth.LinkText>
            </Link>
          </Auth.Text>
        </Auth.FooterWrapper>
      </BaseForm>
    </Auth.FormWrapper>
  );
};
