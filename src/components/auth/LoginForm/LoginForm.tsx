import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import * as Auth from '@app/components/layouts/AuthLayout/AuthLayout.styles';
import { EPath } from '@app/components/router/routerConfig';
import { notificationController } from '@app/controllers/notificationController';
import { useAppDispatch } from '@app/hooks/reduxHooks';
import { doLogin } from '@app/store/slices/authSlice';
import { defaultValidateMessages } from '@app/utils/validate/message';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useNavigate } from 'react-router-dom';
import * as S from './LoginForm.styles';
import { TLoginRequest } from '@app/api/auth.api';
import { AxiosError } from 'axios';

export const LoginForm: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { t } = useTranslation();

  const [isLoading, setLoading] = useState(false);

  const handleSubmit = (values: TLoginRequest) => {
    setLoading(true);
    dispatch(doLogin(values))
      .unwrap()
      .then(() => navigate(EPath.ROOT))
      .catch((err: AxiosError) => {
        console.log('err', err);

        notificationController.error({ message: err.message });
        setLoading(false);
      });
  };

  return (
    <Auth.FormWrapper>
      <BaseForm
        layout="vertical"
        name="login-form"
        onFinish={handleSubmit}
        requiredMark="optional"
        validateMessages={defaultValidateMessages(t)}
        disabled={isLoading}
      >
        <Auth.FormTitle>{t('COMMON.login')}</Auth.FormTitle>
        <S.LoginDescription>{t('login.loginInfo')}</S.LoginDescription>
        <Auth.FormItem name="username" label={t('COMMON.username')} rules={[{ required: true }]}>
          <Auth.FormInput placeholder={t('COMMON.email')} />
        </Auth.FormItem>
        <Auth.FormItem label={t('COMMON.password')} name="password" rules={[{ required: true }]}>
          <Auth.FormInputPassword placeholder={t('COMMON.password')} />
        </Auth.FormItem>
        <Auth.ActionsWrapper>
          {/* <Link to={EPath.FORGOT_PASSWORD}>
            <S.ForgotPasswordText>{t('COMMON.forgotPass')}</S.ForgotPasswordText>
          </Link> */}
        </Auth.ActionsWrapper>
        <BaseForm.Item noStyle>
          <Auth.SubmitButton type="primary" htmlType="submit" loading={isLoading}>
            {t('COMMON.login')}
          </Auth.SubmitButton>
        </BaseForm.Item>
        {/* <BaseForm.Item noStyle>
          <Auth.SocialButton type="default" htmlType="submit">
            <Auth.SocialIconWrapper>
              <GoogleIcon />
            </Auth.SocialIconWrapper>
            {t('login.googleLink')}
          </Auth.SocialButton>
        </BaseForm.Item>
        <BaseForm.Item noStyle>
          <Auth.SocialButton type="default" htmlType="submit">
            <Auth.SocialIconWrapper>
              <FacebookIcon />
            </Auth.SocialIconWrapper>
            {t('login.facebookLink')}
          </Auth.SocialButton>
        </BaseForm.Item> */}
        {/* <Auth.FooterWrapper>
          <Auth.Text>
            {t('login.noAccount')}{' '}
            <Link to={EPath.SIGN_UP}>
              <Auth.LinkText>{t('COMMON.here')}</Auth.LinkText>
            </Link>
          </Auth.Text>
        </Auth.FooterWrapper> */}
      </BaseForm>
    </Auth.FormWrapper>
  );
};
