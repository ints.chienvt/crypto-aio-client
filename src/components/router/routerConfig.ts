export enum EPath {
  ROOT = '/',
  BOT = '/bot',
  EXCHANGE = '/exchange',
  LOGIN = '/auth/login',
  SIGN_UP = '/auth/sign-up',
  FORGOT_PASSWORD = '/auth/forgot-password',
}
