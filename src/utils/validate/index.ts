// Password must be at least 8 characters and must contain at least one letter and one number
export const checkPhoneNumber = /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/g;
export const checkPassword =
  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[A-Za-z\d@$!%*#?&]{8,}$/g;
export const checkRePassword = (password: string) => new RegExp(`^(${password})$`, 'g');
export const checkReEmail = (email: string) => new RegExp(`^(${email})$`, 'g');
export const regexCheckNumber = /^\d+$/;
export const validateEmail = (email: string) => {
  return email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
};
export const checkPositiveNumber = /^(0*[1-9]\d*(\.\d+)?|0+\.\d+)$/;

export const checkSymbolOnURL = /^[^_]+__[^_]+$/;

export const check6NumberCode = /^[0-9]{6}$/;

// Bitcoin addresses are 27-34 alphanumeric digits beginning with 1 or 3 (main net)
// export const checkDepositAddressBTC = /^(1|3)([0-9]|[a-zA-Z]|[\W_]){26,33}$/;
export const checkDepositAddressBTC = null;
// Ethereum addresses are 42 alphanumeric digits beginning with 0x
export const checkDepositAddressETH = /^0x([0-9]|[a-zA-Z]|[\W_]){40}$/;
// FRUITS addresses are 36 alphanumeric digits beginning with  FRUITS-
export const checkDepositAddressFRTS = /^FRUITS-([0-9]|[a-zA-Z]|[\W_]){29}$/;
export const validateDepositAddressArr = [
  {
    symbol: 'BTC',
    validate: checkDepositAddressBTC,
  },
  {
    symbol: 'ETH',
    validate: checkDepositAddressETH,
  },
  {
    symbol: 'USDT',
    validate: checkDepositAddressETH,
  },
  {
    symbol: 'FRTS',
    validate: checkDepositAddressFRTS,
  },
];
