import { translations } from 'locales/translations';
import { TFunction } from 'react-i18next';

export const defaultValidateMessages = (t: TFunction<'translation', undefined>) => ({
  required: t(translations.VALIDATE.DEFAULT.REQUIRED),
  whitespace: t(translations.VALIDATE.DEFAULT.REQUIRED),
  types: {
    email: t(translations.VALIDATE.DEFAULT.TYPES.EMAIL),
    string: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    method: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    array: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    object: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    number: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    date: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    boolean: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    integer: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    float: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    regexp: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    url: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
    hex: t(translations.VALIDATE.DEFAULT.TYPES.DEFAULT),
  },
  number: {
    len: t(translations.VALIDATE.DEFAULT.NUMBER.EQUAL) + ' ' + '${len}',
    min: t(translations.VALIDATE.DEFAULT.NUMBER.MINIMUM) + ' ' + '${min}',
    max: t(translations.VALIDATE.DEFAULT.NUMBER.MAXIMUM) + ' ' + '${max}',
    range: t(translations.VALIDATE.DEFAULT.NUMBER.BETWEEN) + ' ' + '${min}-${max}',
  },
});
