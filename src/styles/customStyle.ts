import { BaseTypography } from '@app/components/common/BaseTypography/BaseTypography';
import styled from 'styled-components';

export const TextLineClamp = styled(BaseTypography.Text)<{ $lineClamp?: number }>`
  -webkit-line-clamp: ${(props) => `${props.$lineClamp ?? 1}`};
  -webkit-box-orient: vertical;
  overflow: hidden;
  display: -webkit-box;
`;
