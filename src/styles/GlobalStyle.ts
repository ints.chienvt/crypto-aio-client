import { createGlobalStyle } from 'styled-components';
import { resetCss } from './resetCss';
import { BREAKPOINTS, FONT_SIZE, FONT_WEIGHT, media } from './themes/constants';
import {
  lightThemeVariables,
  darkThemeVariables,
  commonThemeVariables,
  antOverrideCssVariables,
} from './themes/themeVariables';

export default createGlobalStyle`

  ${resetCss}

  [data-theme='light'],
  :root {
    ${lightThemeVariables}
  }

  [data-theme='dark'] {
    ${darkThemeVariables}
  }

  :root {
    ${commonThemeVariables};
    ${antOverrideCssVariables};
  } 

  [data-no-transition] * {
    transition: none !important;
  }
  
  .range-picker {
    & .ant-picker-panels {
      @media only screen and ${media.xs} and (max-width: ${BREAKPOINTS.md - 0.02}px) {
        display: flex;
      flex-direction: column;
      }
    }
  }

  .search-overlay {
    box-shadow: var(--box-shadow);

    @media only screen and ${media.xs} and (max-width: ${BREAKPOINTS.md - 0.02}px)  {
      width: calc(100vw - 16px);
    max-width: 600px;
    }

    @media only screen and ${media.md} {
      width: 323px;
    }
  }

  a {
    color: var(--primary-color);
    &:hover,:active {
      color: var(--ant-primary-color-hover);
    }
  }
  
  .ant-picker-cell {
    color: var(--text-main-color);
  }

  .ant-picker-cell-in-view .ant-picker-calendar-date-value {
    color: var(--text-main-color);
    font-weight: ${FONT_WEIGHT.bold};
  }

  .ant-picker svg {
    color: var(--text-light-color);
  }

  // notifications start
  .ant-notification-notice {
    width: 24rem;
    padding: 1rem;
    min-height: 4rem;
    
    .ant-notification-notice-with-icon .ant-notification-notice-message {
      margin-bottom: 0;
      margin-left: 2.8125rem;
    }

    .ant-notification-notice-with-icon .ant-notification-notice-description {
      margin-left: 4rem;
      margin-top: 0;
    }

    .ant-notification-notice-icon {
      font-size: 2.8125rem;
      margin-left: 0
    }

    .ant-notification-notice-close {
      top: 1.25rem;
      right: 1.25rem;
    }

    .ant-notification-notice-close-x {
      display: flex;
      font-size: 0.9375rem;
    }

    .notification-without-description {
      .ant-notification-notice-close {
        top: 1.875rem;
      }
      .ant-notification-notice-with-icon .ant-notification-notice-description  {
        margin-top: 0.625rem;
      }
    }
    
    .title {
      font-size: ${FONT_SIZE.xl};
      height: 2rem;
      margin-left: 1.2rem;
      display: flex;
      align-items: center;
      font-weight: ${FONT_WEIGHT.bold};
      -webkit-line-clamp: 1;
      -webkit-box-orient: vertical;
      overflow: hidden;
      display: -webkit-box;

      &.title-only {
        color: var(--text-main-color);
        font-size: ${FONT_SIZE.xs};
        height: 1.7rem;
        line-height: 1.7rem;
        margin-left: 0.75rem;
        font-weight: ${FONT_WEIGHT.semibold};
      }
  }
  
    .description {
      color: #404040;
      font-size: ${FONT_SIZE.xs};
      font-weight: ${FONT_WEIGHT.semibold};
      line-height: 1.1rem;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
      display: -webkit-box;
    }
  
    &.ant-notification-notice-success {
      border: 1px solid var(--success-color);
      background: var(--notification-success-color);
      
      .title {
        color: var(--success-color);
      }
    }
  
    &.ant-notification-notice-info {
      border: 1px solid var(--primary-color);
      background: var(--notification-primary-color);
  
      .title {
        color: var(--primary-color);
      }
    }
  
    &.ant-notification-notice-warning {
      border: 1px solid var(--warning-color);
      background: var(--notification-warning-color);
  
      .title {
        color: var(--warning-color);
      }
    }
  
    &.ant-notification-notice-error {
      border: 1px solid var(--error-color);
      background: var(--notification-error-color);
  
      .title {
        color: var(--error-color);
      }
    }
  
    .success-icon {
      color: var(--success-color);
    }
  
    .info-icon {
      color: var(--primary-color);
    }
  
    .warning-icon {
      color: var(--warning-color);
    }
  
    .error-icon {
      color: var(--error-color);
    }
  }
  
  .ant-menu-inline, .ant-menu-vertical {
    border-right: 0;
  }
  // notifications end

  // segmented start
  .ant-segmented {
    background-color: var(--secondary-background-color);
    border: 1px solid var(--border-base-color);
    padding: 3px;
    &:not(.ant-segmented-disabled):focus, &:not(.ant-segmented-disabled):hover { 
      background: var(--secondary-background-color);
    }
    .ant-segmented-group {
      .ant-segmented-item {
        box-shadow: none;
        color: var(--text-main-color);
        &.ant-segmented-item-selected {
          background-color: var(--background-color);
        }
        &.ant-segmented-item-disabled {
          color: var(--disabled-color);
        }
        .ant-segmented-item-label {
          min-height: 38px;
          line-height: 38px;
        }
      }
      .ant-segmented-thumb {
        background-color: var(--background-color);
      }
    }
  }
  // segmented end

  .ant-input-number {
    .ant-input-number-handler-wrap {
      opacity: 1;
        span {
          display: none;
        }
    }
  }
  .ant-input-number-prefix {
    margin-inline-end: 10px;
  }

  .w-full {
    width: 100%
  }
  .w-half {
    width: 50%
  }
  .flex-center-center {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .ant-empty-normal {
    color: var(--text-main-color);
  }

  .tradingview-widget-container {
    background: none;
  }
  .tradingview-widget-container:before {
    background: none;
  }
`;
