import { TAppBot, TBotGemHunting, TUserBot } from '@app/types/bot';
import { TOptionsQuery } from '@app/types/common';
import queryString from 'query-string';
import { axiosClient } from './axiosClient';
import { AxiosResponse } from 'axios';

export type TRequestCreateUserBot = {
  appBot: string;
  appExchange: string;
};

export type TRequestCreateBotGemHunting = {
  amount: number;
  appCoinPair: string;
  userBot: string;
  price?: number;
  startTime?: Date;
};

const botApi = {
  getAppBots: (params: TOptionsQuery<TAppBot>) => {
    const url = 'app-bots';
    const query = queryString.stringify(params);
    return axiosClient.get(`${url}?${query}`);
  },
  getUserBots: (params: TOptionsQuery<TUserBot>) => {
    const url = 'user-bots';
    const query = queryString.stringify(params);
    return axiosClient.get(`${url}?${query}`);
  },
  createUserBot: (body: TRequestCreateUserBot) => {
    const url = 'user-bots';
    return axiosClient.post<TRequestCreateUserBot, AxiosResponse<TUserBot>>(url, body);
  },
  getBotActives: () => {
    const url = 'bot-gem-huntings';
    return axiosClient.get(url);
  },
  createBotGemHunting: (body: TRequestCreateBotGemHunting) => {
    const url = 'bot-gem-huntings';
    return axiosClient.post<TRequestCreateBotGemHunting, AxiosResponse<TBotGemHunting>>(url, body);
  },
};

export default botApi;
