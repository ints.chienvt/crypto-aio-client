import { axiosClient } from './axiosClient';

export type TAuthData = {
  email: string;
  password: string;
};

export type TSignUpRequest = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

export type TLoginRequest = {
  username: string;
  password: string;
};

export type TLoginResponse = {
  id_token: string;
};

const authApi = {
  login: (params: TLoginRequest) => {
    const url = 'authenticate';
    console.log('params', params);

    return axiosClient.post(url, params);
    // return new Promise((resolve, reject) => {
    //   setTimeout(() => {
    //     resolve({ data: { id_token: 'abc' } });
    //   }, 300);
    // });
  },
  signUp: (params: TSignUpRequest) => {
    const url = 'register';
    return axiosClient.post(url, params);
  },
  logOut: () => {
    const url = 'log-out';
    return axiosClient.post(url);
  },
};

export default authApi;
