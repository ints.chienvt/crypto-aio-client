import { TCoinPair } from '@app/types/coinPair';
import { TAppExchange, TUserExchange } from '@app/types/exchange';

export const mock_appExchanges: TAppExchange[] = [
  {
    id: '1',
    name: 'Binance',
    logo:
      process.env.REACT_APP_ASSETS_BUCKET + '/lightence-activity/unsplash_t1PQ4fYJu7M_ueosw4.webp',
    description: 'A popular cryptocurrency exchange',
    active: true,
    deleted: false,
    createdAt: new Date('2023-03-15'),
    updatedAt: new Date('2023-05-20'),
  },
  {
    id: '2',
    name: 'Bybit',
    logo:
      process.env.REACT_APP_ASSETS_BUCKET + '/lightence-activity/unsplash_t1PQ4fYJu7M_ueosw4.webp',
    description: 'A leading forex exchange',
    active: true,
    deleted: false,
    createdAt: new Date('2023-01-10'),
    updatedAt: new Date('2023-04-18'),
  },
  {
    id: '3',
    name: 'OKX',
    logo:
      process.env.REACT_APP_ASSETS_BUCKET + '/lightence-activity/unsplash_t1PQ4fYJu7M_ueosw4.webp',
    description: 'An emerging stock exchange',
    active: true,
    deleted: false,
    createdAt: new Date('2023-02-20'),
    updatedAt: new Date('2023-06-25'),
  },
  {
    id: '4',
    name: 'Kucoin',
    logo:
      process.env.REACT_APP_ASSETS_BUCKET + '/lightence-activity/unsplash_t1PQ4fYJu7M_ueosw4.webp',
    description: 'A reliable commodities exchange',
    active: true,
    deleted: false,
    createdAt: new Date('2023-04-05'),
    updatedAt: new Date('2023-07-30'),
  },
  {
    id: '5',
    name: 'Mexc',
    logo:
      process.env.REACT_APP_ASSETS_BUCKET + '/lightence-activity/unsplash_t1PQ4fYJu7M_ueosw4.webp',
    description: 'A niche futures exchange',
    active: true,
    deleted: false,
    createdAt: new Date('2023-06-12'),
    updatedAt: new Date('2023-09-28'),
  },
  {
    id: '6',
    name: 'Hubot',
    logo:
      process.env.REACT_APP_ASSETS_BUCKET + '/lightence-activity/unsplash_t1PQ4fYJu7M_ueosw4.webp',
    description: 'A emerging reliable exchange',
    active: true,
    deleted: false,
    createdAt: new Date('2023-06-12'),
    updatedAt: new Date('2023-09-28'),
  },
];

export const mock_exchanges: TUserExchange[] = [
  {
    id: '1',
    accountName: 'user1',
    apiKey: 'API_KEY_1',
    secretKey: 'SECRET_KEY_1',
    passPhrase: 'PASSPHRASE_1',
    isConnected: true,
    deleted: false,
    createdAt: new Date('2023-03-15'),
    updatedAt: new Date('2023-05-20'),
    user: null,
    appExchange: mock_appExchanges[0],
    balance: 1000,
  },
  {
    id: '2',
    accountName: 'user2',
    apiKey: 'API_KEY_2',
    secretKey: 'SECRET_KEY_2',
    passPhrase: 'PASSPHRASE_2',
    isConnected: true,
    deleted: false,
    createdAt: new Date('2023-01-10'),
    updatedAt: new Date('2023-04-18'),
    user: null,
    appExchange: mock_appExchanges[1],
    balance: 500,
  },
  {
    id: '3',
    accountName: 'user3',
    apiKey: 'API_KEY_3',
    secretKey: 'SECRET_KEY_3',
    passPhrase: 'PASSPHRASE_3',
    isConnected: true,
    deleted: false,
    createdAt: new Date('2023-02-20'),
    updatedAt: new Date('2023-06-25'),
    user: null,
    appExchange: mock_appExchanges[2],
    balance: 1500,
  },
  {
    id: '4',
    accountName: 'user4',
    apiKey: 'API_KEY_4',
    secretKey: 'SECRET_KEY_4',
    passPhrase: 'PASSPHRASE_4',
    isConnected: true,
    deleted: false,
    createdAt: new Date('2023-04-05'),
    updatedAt: new Date('2023-07-30'),
    user: null,
    appExchange: mock_appExchanges[3],
    balance: 200,
  },
];

export const mock_coinPairs: TCoinPair[] = [
  {
    id: '1',
    name: 'BTC/USDT',
    symbol: 'BTCUSDT',
    base: 'BTC',
    quote: 'USDT',
    active: true,
    deleted: false,
    createdAt: new Date('2023-03-15'),
    updatedAt: new Date('2023-05-20'),
    appExchange: mock_appExchanges[0],
  },
  {
    id: '2',
    name: 'ETH/BTC',
    symbol: 'ETHBTC',
    base: 'ETH',
    quote: 'BTC',
    active: true,
    deleted: false,
    createdAt: new Date('2023-01-10'),
    updatedAt: new Date('2023-04-18'),
    appExchange: mock_appExchanges[1],
  },
  {
    id: '3',
    name: 'XRP/USDT',
    symbol: 'XRPUSDT',
    base: 'XRP',
    quote: 'USDT',
    active: true,
    deleted: false,
    createdAt: new Date('2023-02-20'),
    updatedAt: new Date('2023-06-25'),
    appExchange: mock_appExchanges[2],
  },
  {
    id: '4',
    name: 'LTC/BTC',
    symbol: 'LTCBTC',
    base: 'LTC',
    quote: 'BTC',
    active: true,
    deleted: false,
    createdAt: new Date('2023-04-05'),
    updatedAt: new Date('2023-07-30'),
    appExchange: mock_appExchanges[3],
  },
  {
    id: '5',
    name: 'BCH/USDT',
    symbol: 'BCHUSDT',
    base: 'BCH',
    quote: 'USDT',
    active: true,
    deleted: false,
    createdAt: new Date('2023-06-12'),
    updatedAt: new Date('2023-09-28'),
    appExchange: mock_appExchanges[3],
  },
];

export const mock_User = {
  id: '1',
  firstName: 'Chien',
  lastName: 'Vu',
  login: 'chienvt',
  email: 'chienvt@ints.vn',
  langKey: 'en',
  activated: true,
  authorities: [],
  createdDate: new Date(),
};
