import { TUser } from '@app/types/user';
import { axiosClient } from './axiosClient';

const userApi = {
  getCurrentUser: () => {
    const url = 'account';
    return axiosClient.get(url);
  },
  updateUser: (params: TUser) => {
    const url = 'account';
    return axiosClient.patch(url, params);
  },
};

export default userApi;
