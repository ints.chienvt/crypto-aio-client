import { TAppExchange, TUserExchange } from '@app/types/exchange';
import { axiosClient } from './axiosClient';
import { TOptionsQuery } from '@app/types/common';
import queryString from 'query-string';
import { AxiosResponse } from 'axios';

export type TRequestAddExchange = {
  appExchange: string;
  apiKey: string;
  secretKey?: string;
  passPhrase?: string;
};

const exchangeApi = {
  getAppExchanges: (params: TOptionsQuery<TAppExchange>) => {
    const url = 'app-exchanges';
    const query = queryString.stringify(params);
    return axiosClient.get(`${url}?${query}`);
  },
  getExchanges: () => {
    const url = 'user-exchanges';
    return axiosClient.get(url);
  },
  addExchange: (body: TRequestAddExchange) => {
    const url = 'user-exchanges-extend';
    return axiosClient.post<TRequestAddExchange, AxiosResponse<TAppExchange>>(url, body);
  },
};

export default exchangeApi;
