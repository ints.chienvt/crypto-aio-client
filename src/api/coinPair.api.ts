import { TOptionsQuery } from '@app/types/common';
import { axiosClient } from './axiosClient';
import { TCoinPair } from '@app/types/coinPair';
import queryString from 'query-string';

const coinPairApi = {
  getCoinPairs: (params: TOptionsQuery<TCoinPair>) => {
    const url = 'app-coin-pairs';
    const query = queryString.stringify(params);
    return axiosClient.get(`${url}?${query}`);
  },
};

export default coinPairApi;
