export type CategoryType = 'apps' | 'forms' | 'charts' | 'auth' | 'data tables' | 'maps';

interface Category {
  name: CategoryType;
  title: string;
}

export const categoriesList: Category[] = [
  {
    name: 'apps',
    title: 'COMMON.apps',
  },
  {
    name: 'auth',
    title: 'COMMON.auth',
  },
  {
    name: 'forms',
    title: 'COMMON.forms',
  },
  {
    name: 'data tables',
    title: 'COMMON.dataTables',
  },
  {
    name: 'charts',
    title: 'COMMON.charts',
  },
  {
    name: 'maps',
    title: 'COMMON.maps',
  },
];
