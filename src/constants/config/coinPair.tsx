import { TCoinPairDirectionType } from '@app/interfaces/interfaces';

interface CoinPairDirectionItem {
  name: TCoinPairDirectionType;
  title: string;
  color: 'long' | 'short';
}

export const coinPairDirections: CoinPairDirectionItem[] = [
  {
    name: 'long',
    title: 'COMMON.LONG',
    color: 'long',
  },
  {
    name: 'short',
    title: 'COMMON.SHORT',
    color: 'short',
  },
];
