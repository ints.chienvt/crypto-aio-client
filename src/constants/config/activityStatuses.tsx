import React from 'react';
import { DollarOutlined, PlusOutlined, ReadOutlined } from '@ant-design/icons';
import { TCoinPairDirectionType } from '@app/interfaces/interfaces';

interface ActivityStatusItem {
  name: TCoinPairDirectionType;
  title: string;
  color: 'long' | 'short';
  icon: React.ReactNode;
}

export const activityStatuses: ActivityStatusItem[] = [
  {
    name: 'long',
    title: 'COMMON.long',
    color: 'long',
    icon: <DollarOutlined />,
  },
  {
    name: 'short',
    title: 'COMMON.short',
    color: 'short',
    icon: <ReadOutlined />,
  },
];
