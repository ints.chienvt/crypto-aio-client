import { LANGUAGE_DE, LANGUAGE_EN } from '@app/i18n';
import { LanguageType } from '@app/interfaces/interfaces';

interface Language {
  id: string;
  name: LanguageType;
  title: string;
  countryCode: string;
}

export const languages: Language[] = [
  {
    id: '1',
    name: LANGUAGE_EN,
    title: 'English',
    countryCode: 'gb',
  },
  // {
  //   id: '2',
  //   name: LANGUAGE_DE,
  //   title: 'German',
  //   countryCode: 'de',
  // },
];
